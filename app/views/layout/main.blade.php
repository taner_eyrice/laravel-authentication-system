<!DOCTYPE html>
<html>
<head>
    <title>Grapehic Portal</title>
</head>
<body>

    @if(Session::has('global'))
        <p>{{ Session::get('global') }}</p>
    @endif

    @include('layout.navigation')
    @yield('content')
</body>
</html>